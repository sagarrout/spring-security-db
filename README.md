# Spring Security Using Database

## Technology stack
1. Spring Boot (1.5.10)
2. PostgreSQL Database // User credentials and basic role
3. Lombok // Why I am Using ? I like it. No boiler plate code for getters, setters and all.


## Run or deploy application
1. Run from IDE (Intellij IDEA or Eclipse or Apache Netbeans)
2. Run from command prompt using mvn ```mvn clean compile spring-boot:run```


### Application contains two endpoints
1. Unsecured (http://localhost:8080/rest/hello-github)
2. Secured (http://localhost:8080/rest/secured/hello-blackOcean) with admin access
