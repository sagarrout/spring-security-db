package com.example.springsecuritydb.service;

import com.example.springsecuritydb.domain.User;
import com.example.springsecuritydb.repository.UserRepository;
import com.example.springsecuritydb.security.CustomUserDetails;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String emailId) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByEmailId(emailId);

        optionalUser.orElseThrow(() -> new UsernameNotFoundException("user not found !!!"));

        return optionalUser.map(CustomUserDetails::new).get();
    }
}
