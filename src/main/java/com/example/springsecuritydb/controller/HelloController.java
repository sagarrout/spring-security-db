package com.example.springsecuritydb.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/rest/hello-github") // Cam be accessible without login
    public String helloGithub() {
        return "Hello Github";
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/rest/secured/hello-blackOcean") // Need login access with admin credentials
    public String helloBlackOceanGithub() {
        return "Hello blackOcean";
    }
}
