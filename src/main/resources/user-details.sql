-- Bootup user details in database

INSERT INTO public.roles (id, description, name) VALUES (1, 'Administrator Access', 'ADMIN');

INSERT INTO public.users (id, active, age, creation_date, email_id, first_name, last_name, password, updated_date) VALUES (1, true, 24, '2018-03-17 12:12:17', 'sagar@sagar.com', 'Sagar', 'Rout', 'sagar', '2018-03-17 12:12:17');

INSERT INTO public.users_roles (users_id, roles_id) VALUES (1, 1);
